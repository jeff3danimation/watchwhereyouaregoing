﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    public ParticleSystem turboSmoke;
    public float speed = 5f;
    private GameObject target;
    public GameObject indicator;
    public Camera cam;

    public Text LifeText;
    public Text BoostText;
    public Text PowerupText;
    public Text GameOverText;

    public bool hasPowerup = false;
    private float powerupStr = 15f;

    private int threeGetOutOfJailBuffs = 3;
    private int threeLives = 3;

    private LayerMask indicatorLM;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        target = GameObject.Find("Target");
        indicatorLM = LayerMask.NameToLayer("Indicator");
        HideIndicator();
        GameOverText.enabled = false;
        turboSmoke.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        indicator.transform.position = transform.position;
        turboSmoke.transform.position = transform.position;

        float forwardInput = Input.GetAxis("Vertical");
        playerRb.AddForce(target.transform.forward * forwardInput * speed);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (threeGetOutOfJailBuffs > 0)
            {
                playerRb.AddForce(target.transform.forward * 30, ForceMode.VelocityChange);
                threeGetOutOfJailBuffs--;
                BoostText.text = threeGetOutOfJailBuffs.ToString();
                if (turboSmoke.isPlaying)
                {
                    turboSmoke.Stop();
                }
                turboSmoke.Play();
                Debug.LogFormat("You have used a powerful buff. You have {0} more.", threeGetOutOfJailBuffs);
                StartCoroutine(HaltingCoroutine());
            }
        }

        if (transform.position.y < -10)
        {
            if (threeLives > 0)
            {
                playerRb.Sleep();
                turboSmoke.Stop();
                transform.position = new Vector3(0, 0, 0);
                threeLives--;
                LifeText.text = threeLives.ToString();
                Debug.LogFormat("You have used a life. You have {0} more.", threeLives);
            }

            else
            {
                Destroy(gameObject);
                turboSmoke.Stop();
                Debug.Log("Game Over");
                GameOverText.enabled = true;
            }
        }
    }

    IEnumerator HaltingCoroutine()
    {
        yield return new WaitForSeconds(3);
        turboSmoke.Stop();
        Debug.Log("VOILA");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Powerup"))
        {
            if (hasPowerup == false)
            {
                hasPowerup = true;
                Destroy(other.gameObject);
                ShowIndicator();
                StartCoroutine(PowerupCountdownRoutine());
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && hasPowerup)
        {
            Rigidbody enemyRb = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 awayFromPlayer = (collision.gameObject.transform.position - transform.position);

            // BOOM
            enemyRb.AddForce(awayFromPlayer * powerupStr, ForceMode.Impulse);
        }
    }

    // Turn on the bit using an OR operation:
    private void ShowIndicator()
    {
        cam.cullingMask |= 1 << indicatorLM;
        PowerupText.enabled = true;
    }

    // Turn off the bit using an AND operation with the complement of the shifted int:
    private void HideIndicator()
    {
        cam.cullingMask &= ~(1 << indicatorLM);
        PowerupText.enabled = false;
    }

    // Toggle the bit using a XOR operation:
    private void ToggleIndicator()
    {
        cam.cullingMask ^= 1 << indicatorLM;
        PowerupText.enabled ^= true;
    }

    IEnumerator PowerupCountdownRoutine()
    {
        yield return new WaitForSeconds(5);
        ToggleIndicator();

        for (int i=0; i < 10; i++)
        {
            yield return new WaitForSeconds(.5f);
            ToggleIndicator();
            i++;
        }

        for (int i = 0; i < 10; i++)
        {
            yield return new WaitForSeconds(.25f);
            ToggleIndicator();
            i++;
        }

        // off
        yield return new WaitForSeconds(1);
        hasPowerup = false;
        HideIndicator();
    }


}
