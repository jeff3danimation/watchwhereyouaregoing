﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float speed;
    public bool clockwise = true;
    private float internalSpeedMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        float pluckedRange = Random.Range(0, 10) * 0.5f;

        if (clockwise)
        {
            internalSpeedMultiplier = pluckedRange;
        }
        else
        {
            internalSpeedMultiplier = -pluckedRange;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, 5, 0) * speed * internalSpeedMultiplier * Time.deltaTime);
    }
}
