﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnManager : MonoBehaviour
{
    public GameObject enemyPrefab;
    public GameObject powerup;
    public int amountOfPowerups;
    public int amountOfEnemiesRequested;
    public int waveNumber = 1;
    public Text WaveNumberText;

    private int numberOfEnemiesToSpawn;

    private float spawnRange = 9;

    // Start is called before the first frame update
    void Start()
    {
        GenerateMultiplePowerups(amountOfPowerups);

        // Vector3 rand = GenerateRandom();
        // Instantiate(enemyPrefab, rand, enemyPrefab.transform.rotation);
        numberOfEnemiesToSpawn = amountOfEnemiesRequested + waveNumber;
        Debug.Log("Wave Number: " + waveNumber.ToString());
        SpawnEnemyWave(numberOfEnemiesToSpawn);
    }

    void Update()
    {
        int enemyCount = FindObjectsOfType<Enemy>().Length;
        if (enemyCount == 0)
        {
            numberOfEnemiesToSpawn++;
            waveNumber++;
            Debug.Log("Wave Number: " + waveNumber.ToString());
            WaveNumberText.text = waveNumber.ToString();
            SpawnEnemyWave(numberOfEnemiesToSpawn);
            GenerateMultiplePowerups(1);
        }
    }

    void SpawnEnemyWave(int enemies)
    {
        for (int i=0; i < enemies; i++)
        {
            Vector3 rand = GenerateRandom();
            Instantiate(enemyPrefab, rand, enemyPrefab.transform.rotation);
        }
    }

    private Vector3 GenerateRandom()
    {
        float spawnPosX = Random.Range(-spawnRange, spawnRange);
        float spawnPosZ = Random.Range(-spawnRange, spawnRange);
        Vector3 randomPos = new Vector3(spawnPosX, 0, spawnPosZ);

        return randomPos;
    }

    private void GenerateMultiplePowerups(int amount)
    {
        for (int i = 0; i < amount; i++)
        {
            Vector3 randomLocation = GenerateRandom();
            Instantiate(powerup, randomLocation, powerup.transform.rotation);
        }
    }
}
