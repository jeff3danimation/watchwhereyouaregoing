﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : MonoBehaviour
{
    private float speed;
    private float height = .5f;
    private Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        //get the objects current position and put it in a variable so we can access it later with less code
        pos = transform.position;
        speed = Random.Range(0f, 2f);
    }

    // Update is called once per frame
    void Update()
    {
        //calculate what the new Y position will be
        float newY = Mathf.Sin(Time.time * speed);
        //set the object's Y to the new calculated Y
        transform.position = new Vector3(pos.x, ( newY + 12 ) * height, pos.z);
    }
}
